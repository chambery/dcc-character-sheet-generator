import { CSSProperties } from "react"

export type Rectangle = {
  left: number,
  top: number,
  height?: number,
  width?: number
}

export type Attr = {
  break?: boolean
  pos?: boolean
  label?: string
  style?: CSSProperties
  rect: Rectangle
}

export type Character = {
  [index: string]: Attr
}