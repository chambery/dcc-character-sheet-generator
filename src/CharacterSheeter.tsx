const images = require.context('./images', true)
import * as React from 'react'
import * as _ from 'lodash'
import { CSSProperties } from 'react'
import './stylesheet.css'
// import { fetchCharacterData } from './actions'
import { Container } from 'flux/utils'
import reducer from './reducer'
import { fetchCharacterData } from './actions'
import layouts from './layouts'
import { Attr, Character } from './types'
type Props = {
  match: {
    params: {
      sheetname: string
    }
  }
}

type State = {
  characters: any[]
}

type datatype = any[]

const right = 1653
const down = 1275


// const createURL = 'https://purplesorcerer.com/create.php?oc=rulebook&mode=3d6&stats=&hp=normal&at=normal&display=json&sc=1'



class CharacterSheeter extends React.Component<Props, State> {
  // constructor(props: Props) {
  //   super(props)
  // }

  static getStores() { return [reducer] }

  static calculateState(prevState: State, props: Props) {

    const characters = reducer.getState().get('characterData') || []

    return {
      characters: characters
    }
  }

  componentDidMount() {
    fetchCharacterData()
  }

  splitLuckSign(s: string) {
    const oParen = s.indexOf('(')
    const cParen = s.indexOf(')')
    let sign = s.slice(0, oParen)
    let mod = s.slice(oParen + 1, cParen) + s.slice(cParen + 1)
    console.log('\t', s, sign, mod);

    return {
      luckySign: sign,
      luckyMod: mod
    }
  }

  render() {
    const characters = this.state.characters
    // @ts-ignore
    const right = _.get(layouts[this.props.match.params.sheetname], 'offset.right') || 1653
    // @ts-ignore
    const down = _.get(layouts[this.props.match.params.sheetname], 'offset.down') || 1275

    return (
      <div>
        <img style={{ zIndex: -1 }} src={images('./' + this.props.match.params.sheetname + '.png')} alt='boop' />
        <div style={{
          fontFamily: 'dpdorkdiarymedium',
          fontWeight: 'bold',
          fontSize: '42px'

        }}>
          {
            characters.length > 0
            &&

            _.map([{ right: 1, down: 1 }, { right: right, down: 1 }, { right: 1, down: down }, { right: right, down: down }], (offset: { right: number, down: number }, idx: number) => {

              const character = _.assign({}, characters[idx], this.splitLuckSign(characters[idx]['luckySign']), {startingFunds: characters[idx]['startingFunds'].slice(0, characters[idx]['startingFunds'].indexOf(' '))})
              console.log(character);
              return (
                //@ts-ignore
                _.map(layouts[this.props.match.params.sheetname].attributes, (layout: Attr, attr: string) => {
                   
                  return (
                    <div key={attr + idx} style={_.assign({}, {
                      position: 'absolute',
                      left: `${layout.rect.left + offset.right}px`,
                      top: `${layout.rect.top + offset.down}px`,
                      height: `${layout.rect.height}px`,
                      width: `${layout.rect.width}px`,
                      // display: 'flex',
                      alignItems: 'flex-end',
                      border: '1px dotted silver'
                    }, layout.style, layout.break && _.indexOf(character[attr], ' ') > -1 && { fontSize: '24px' })}>
                      {layout.label}
                      {
                        layout.pos
                        &&
                        Number.isInteger(parseInt(character[attr]))
                        &&
                        parseInt(character[attr]) > 0
                        &&
                        '+'
                      }
                      {
                        character[attr]
                      }
                    </div>
                  )
                })
              )
            })
          }
        </div>
      </div>
    )

  }
}
const container = Container.create(CharacterSheeter, { withProps: true })
export default container