import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { HashRouter, Route } from 'react-router-dom';
import CharacterSheeter from './CharacterSheeter'
import SheetSelector from './SheetSelector'
import './index.css';
import './stylesheet.css';

ReactDOM.render(
  <HashRouter>
    <div>
      <Route exact={true} path="/" component={SheetSelector} />
      <Route exact={true} path="/sheet/:sheetname" component={CharacterSheeter} />
    </div>
  </HashRouter>,
  document.getElementById('root') as HTMLElement
)

