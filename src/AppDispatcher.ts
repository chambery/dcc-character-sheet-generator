import { Dispatcher } from 'flux'
import { Action } from './ProductMgmt'

class AppDispatcher extends Dispatcher<Action> {
  /**
   * Dispatches three actions for an async operation represented by promise.
   */
  dispatchAsync(promise: Promise<unknown>, types: any, payload?: any) {
    const { request, success, failure } = types
    if (request) {
      instance.dispatch({
        type: request,
        payload: Object.assign({}, payload)
      })
    }

    return promise.then(
      (response: any) => {
        instance.dispatch({
          type: success,
          payload: Object.assign({}, payload, { response })
        })
      },
      (error: Object) => instance.dispatch({ type: failure, payload: Object.assign({}, payload, { error }) })
    )
  }

}

const instance: AppDispatcher = new AppDispatcher()

let _dispatch = instance.dispatch.bind(instance)


export default instance

// So we can conveniently do, `import {dispatch} from './AppDispatcher'`
export const dispatch = _dispatch
