import * as _ from 'lodash'
import * as request from 'superagent'
import response from './response'

const api = {
  get: (url: string, queryArgs: object = {}, afterFn: Function = (result: any) => result) => {
    return new Promise((resolve, reject) => {
      // request.get(url)
      //   .set('Accept', 'application/json')
      //   .type('form')
      //   .query(queryArgs)
      //   .redirects(0)
      //   .end(function (err, res) {
      //     console.log(`   ${url}.end`, err, res, _.get(res, ['text']))
      //     let data = res.text
      //     if (err == null && data) {
      //       const result = afterFn(data)
      //       return resolve(result)
      //     } else {
      //       return reject(res)
      //     }
      //   })
      return resolve(response)
    },
    )
  },

  fetchCharacterData: () => {
    // return response
    return api.get('http://localhost:8080/https://purplesorcerer.com/create.php', { oc: 'rulebook', mode: '3d6', hp: 'normal', at: 'normal', display: 'json', sc: 1 })
  }
}

export default api