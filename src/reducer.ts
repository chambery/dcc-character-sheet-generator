import { ReduceStore } from 'flux/utils'
import { Map } from 'immutable'
// import * as _ from 'lodash'
import Dispatcher from './AppDispatcher'

type State = Map<string, any>

type Action = {
  type: string
  payload: {
    response: any
  }
}

class Reducer extends ReduceStore<State, any> {

  getInitialState() {
    return Map({
      characterData: []
    })
  }

  reduce(state: State, action: Action): State {
    console.log(action.type, action.payload)

    switch (action.type) {
      case 'get_character_data_success': return state.set('characterData', action.payload.response['characters'])
      default: return state
    }
  }

  // setOptionActions(state: State, result: OptionAction[]) {
  //   console.log('setting optinoactions', result)

  //   return  result)
  // }

  // set(state: State, result: any, collectionName: string, typeName: string, idName: string) {
  //   const items = state.get(collectionName).slice()
  //   const idx = _.findIndex(items, (i: any) => result[typeName] && i[idName] == result[typeName][idName])
  //   if (idx > -1) {
  //     items.splice(idx, 1, result[typeName])
  //   } else {
  //     items.push(result[typeName])
  //   }

  //   return state.merge({
  //     [typeName]: result[typeName],
  //     breadcrumb: result.breadcrumb,
  //     [collectionName]: items,
  //     rebuilding: result.rebuilding
  //   })
  // }
}



const instance = new Reducer(Dispatcher)

export default instance
