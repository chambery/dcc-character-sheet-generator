import Dispatcher from './AppDispatcher'
import api from './api'

export function fetchCharacterData() {
  return Dispatcher.dispatchAsync(
    api.fetchCharacterData(),
    {
      success: 'get_character_data_success',
      failure: 'get_character_data_failure',
    }
  )
}