

export type ActionResult = {
  request?: string
  success: string
  failure: string
}

export type Action = {
  type: string
  payload: any
}

