import * as React from 'react';
const images = require.context('./images', true)
import { Container } from 'flux/utils'
import reducer from './reducer'
import * as _ from 'lodash'
import { listReactFiles } from 'list-react-files'

type Props = {
}

type State = {
}

class SheetSelector extends React.Component<Props, State> {

  static getStores() { return [reducer] }

  static calculateState(prevState: State, props: Props) { }

  render() {
    return (
      <div style={{ columnCount: 3 }}>
        {/* {
          _.map(listReactFiles(images), imgName => {
            console.log(imgName);
            
          })
        } */}
        <a href='#/sheet/goodman'><img src={images('./goodman-single.png')} width={400} /></a>
        <a href='#/sheet/skin'><img src={images('./skin-single.png')} width={400} /></a>
        <a href='#/sheet/elf-pink'><img src={images('./elf-pink-single.png')} width={400} /></a>
        <a href='#/sheet/beigey-blue'><img src={images('./beigey-blue-single.png')} width={400} /></a>
      </div>
    )
  }
}

const container = Container.create(SheetSelector, { withProps: true })
export default container