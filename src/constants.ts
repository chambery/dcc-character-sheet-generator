export const TEXTAREA = { componentClass: 'textarea' }
export const TEXT = { type: 'text' }

export const GET_PRODUCTS_SUCCESS = 'get_products_success'
export const GET_PRODUCTS_FAILURE = 'get_products_failure'

export const GET_SERVICES_SUCCESS = 'get_services_success'
export const GET_SERVICES_FAILURE = 'get_services_failure'

export const GET_SERVICE_SUCCESS = 'get_service_success'
export const GET_SERVICE_FAILURE = 'get_service_failure'

export const GET_SERVICE_VENDORS_SUCCESS = 'get_service_vendors_success'
export const GET_SERVICE_VENDORS_FAILURE = 'get_service_vendors_failure'

export const GET_SERVICE_VENDOR_SUCCESS = 'get_service_vendor_success'
export const GET_SERVICE_VENDOR_FAILURE = 'get_service_vendor_failure'

export const GET_OPTIONS_SUCCESS = 'get_options_success'
export const GET_OPTIONS_FAILURE = 'get_options_failure'

export const GET_PARTITION_OPTIONS_SUCCESS = 'get_partition_options_success'
export const GET_PARTITION_OPTIONS_FAILURE = 'get_partition__options_failure'

export const GET_CUSTOM_ORGS_SUCCESS = 'get_custom_orgs_success'
export const GET_CUSTOM_ORGS_FAILURE = 'get_custom_orgs_failure'

export const GET_OPTION_SUCCESS = 'get_option_success'
export const GET_OPTION_FAILURE = 'get_option_failure'

export const GET_GROUPS_SUCCESS = 'get_groups_success'
export const GET_GROUPS_FAILURE = 'get_groups_failure'

export const GET_GROUP_SUCCESS = 'get_group_success'
export const GET_GROUP_FAILURE = 'get_group_failure'

export const GET_PRICES_SUCCESS = 'get_prices_success'
export const GET_PRICES_FAILURE = 'get_prices_failure'

export const GET_PRICE_SUCCESS = 'get_price_success'
export const GET_PRICE_FAILURE = 'get_price_failure'

export const GET_GLOBAL_SEARCH_SUCCESS = 'get_global_search_success'
export const GET_GLOBAL_SEARCH_FAILURE = 'get_global_search_failure'

export const GET_USERS_SUCCESS = 'get_users_success'
export const GET_USERS_FAILURE = 'get_users_failure'

export const GET_DETAIL_CONTAINERS_SUCCESS = 'get_detail_containers_success'
export const GET_DETAIL_CONTAINERS_FAILURE = 'get_detail_containers_failure'

export const SAVE_GROUP_ORDER_SUCCESS = 'save_group_order_success'
export const SAVE_GROUP_ORDER_FAILURE = 'save_group_order_failure'

export const SAVE_OPTION_ORDER_SUCCESS = 'save_option_order_success'
export const SAVE_OPTION_ORDER_FAILURE = 'save_option_order_failure'

export const SAVE_SERVICE_SUCCESS = 'save_service_success'
export const SAVE_SERVICE_FAILURE = 'save_service_failure'

export const SAVE_SERVICE_VENDOR_SUCCESS = 'save_service_vendor_success'
export const SAVE_SERVICE_VENDOR_FAILURE = 'save_service_vendor_failure'

export const SAVE_SERVICE_ACTION_SUCCESS = 'save_service_action_success'
export const SAVE_SERVICE_ACTION_FAILURE = 'save_service_action_failure'

export const SAVE_GROUP_SUCCESS = 'save_group_success'
export const SAVE_GROUP_FAILURE = 'save_group_failure'

export const SAVE_OPTION_SUCCESS = 'save_option_success'
export const SAVE_OPTION_FAILURE = 'save_option_failure'

export const SAVE_SUBOPTION_SUCCESS = 'save_suboption_success'
export const SAVE_SUBOPTION_FAILURE = 'save_suboption_failure'

export const SAVE_PRICE_SUCCESS = 'save_price_success'
export const SAVE_PRICE_FAILURE = 'save_price_failure'

export const GET_QUANTITY_LOCK_SUCCESS = 'get_quantity_lock_success'
export const GET_QUANTITY_LOCK_FAILURE = 'get_quantity_lock_failure'

export const GET_SERVICE_ACTION_SUCCESS = 'get_service_action_success'
export const GET_SERVICE_ACTION_FAILURE = 'get_service_action_failure'

export const SAVE_OPTION_ACTION_SUCCESS = 'save_option_action_success'
export const SAVE_OPTION_ACTION_FAILURE = 'save_option_action_failure'

export const GET_OBJECT_ACTIONS_SUCCESS = 'get_object_actions_success'
export const GET_OBJECT_ACTIONS_FAILURE = 'get_object_actions_failure'

export const GET_OBJECT_RULES_SUCCESS = 'get_object_rules_success'
export const GET_OBJECT_RULES_FAILURE = 'get_object_rules_failure'

export const SAVE_RULES_SUCCESS = 'save_rules_success'
export const SAVE_RULES_FAILURE = 'save_rules_failure'

export const SAVE_PARTIALS_SUCCESS = 'save_partials_success'
export const SAVE_PARTIALS_FAILURE = 'save_partials_failure'

export const GET_VENDOR_TREE_SUCCESS = 'get_vendor_tree_success'
export const GET_VENDOR_TREE_FAILURE = 'get_vendor_tree_failure'

export const GET_SERVICE_ACTIONS_SUCCESS = 'get_service_actions_success'
export const GET_SERVICE_ACTIONS_FAILURE = 'get_service_actions_failure'

export const REBUILD_DEPENDENCIES = 'rebuild_dependencies'
export const REBUILD_DEPENDENCIES_SUCCESS = 'rebuild_dependencies_success'
export const REBUILD_DEPENDENCIES_FAILURE = 'rebuild_dependencies_failure'

export const GET_INTERNAL_TARIFF_CODE = 'get_internal_tariff_code'
export const GET_INTERNAL_TARIFF_CODE_SUCCESS = 'get_internal_tariff_code_success'
export const GET_INTERNAL_TARIFF_CODE_FAILURE = 'get_internal_tariff_code_failure'
export const CLEAR_INTERNAL_TARIFF_CODE = 'clear_internal_tariff_code'

export const GET_BUNDLES_SUCCESS = 'fetch_bundles_success'
export const GET_BUNDLES_FAILURE = 'fetch_bundles_failure'

export const GET_OBJECT_BUNDLES_SUCCESS = 'fetch_object_bundles_success'
export const GET_OBJECT_BUNDLES_FAILURE = 'fetch_object_bundles_failure'

export const GET_BUNDLE_SUCCESS = 'fetch_bundle_success'
export const GET_BUNDLE_FAILURE = 'fetch_bundle_failure'

export const SAVE_BUNDLE_SUCCESS = 'save_bundle_success'
export const SAVE_BUNDLE_FAILURE = 'save_bundle_failure'

export const DELETE_BUNDLE_SUCCESS = 'delete_bundle_success'
export const DELETE_BUNDLE_FAILURE = 'delete_bundle_failure'

export const SAVE_OBJECT_BUNDLES_SUCCESS = 'save_object_bundles_success'
export const SAVE_OBJECT_BUNDLES_FAILURE = 'save_object_bundles_failure'

export const SET_SERVICE = 'set_service'

export const SERVICES = 'services'
export const SERVICE = 'service'
export const SERVICE_VENDOR = 'service_vendor'
export const VENDORS = 'vendors'
export const VENDOR = 'vendor'
export const SERVICE_ACTION = 'service_action'
export const BUNDLE = 'bundle'
export const SERVICE_ACTIONS = 'service_actions'
export const GROUPS = 'groups'
export const GROUP = 'group'
export const OPTIONS = 'options'
export const PARENT_OPTION = 'parent_option'
export const OPTION = 'option'
export const PRICES = 'prices'
export const PRICE = 'price'

export const EDITED = 'green'
export const UNCHANGED = 'black'

export const MAX_RESULTS = 10

export const columns = {
  // io: { db: 'so_incoming_outgoing', display: 'IO', maxLength: 1},
  // voa_id: { db: 'voa_id', display: 'ID' },
  voa_name: { db: 'voa_name', display: 'Name' },

}

export const toOp = {
  RVL: 'Is Revealed By',
  ACT: 'Is Activated By',
  INV: 'Is Hidden By',
  REQ: 'Is Required By'
}

export const fromOp = {
  RVL: 'Reveals',
  ACT: 'Activates',
  INV: 'Hides',
  REQ: 'Requires'
}

export const opIconMap ={
  RVL: 'eye',
  ACT: 'check',
  INV: 'ban',
  REQ: 'exclamation-circle'
}
